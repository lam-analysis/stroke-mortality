---
title: Prognostic model on stroke mortality
output:
  pdf_document:
    keep_md: true
bibliography: ref.bib
header-includes:
- \usepackage{longtable}
---



# Methods

In this multivariable analysis, we compared several theoretically-driven models
to explain mortality in ischemic stroke. First we identified the factors
affecting mortality in stroke, including:

- History of uncontrolled diabetes or hypertension
- The presence of heart disease or renal disease
- Patient age and sex
- Blood pressure, denoted by systole and diastole
- Glasgow Coma Scale (GCS), separated into a measure of Eye (E), Movement (M),
  and Verbal (V)
- Body Mass Index (BMI)
- Blood level of uric acid

Considering the needs of adjustment in a high-dimensional data (i.e. data with
multiple variables), the current statistical literature strongly recommend
against using a bivariate selection [@Sun1996]. The lack of statistical rigor
provided by variable selection using a stepwise regression, either using the
p-value, AIC, or BIC, may not improve our model [@Chowdhury2020]. Although it
merits some purpose in modelling [@Sauerbrei2020], we remained to opt for a
theory-based model.

Therefore, we included identified factors into a generalized linear model with
logit link function and performed a logistic regression to predict mortality.
Then we assessed each variable Odds Ratio (OR) alongside its confidence
interval. By considering expert's opinion on clinical relevance and data
suitability, we streamlined the analysis to obtain a parsimonious model.

Due to the presence of ambiguous operational definition in verbal (from GCS),
we concluded the need of deriving the verbal measure into speech coherence. We
consider coherent speech in patients whose verbal $V \geqslant 4$. This way, we
obtain a nominal boolean variable to represent the verbal measure. By doing so,
we prevented the loss of hundreds of data due to removal if we use the previous
verbal measure.

Our final model includes the following variables:

- Age
- Heart disease
- Renal disease
- Eye
- Movement
- Verbal coherence
- BMI

# Results



Our analysis included 3479 participants in total, with
154 mortalities,
999 having a heart disease,
421 having a renal disease,
and 511 verbally incoherent.

## Descriptive Statistics


\begin{longtable}[t]{lrrrrrr}
\caption{\label{tab:num.res}Descriptive statistics on numeric variables}\\
\toprule
\multicolumn{4}{c}{ } & \multicolumn{2}{c}{95\% CI} & \multicolumn{1}{c}{ } \\
\cmidrule(l{3pt}r{3pt}){5-6}
  & Event & No event & d & 2.5\% & 97.5\% & p\\
\midrule
\endfirsthead
\caption[]{Descriptive statistics on numeric variables \textit{(continued)}}\\
\toprule
\multicolumn{4}{c}{ } & \multicolumn{2}{c}{95\% CI} & \multicolumn{1}{c}{ } \\
\cmidrule(l{3pt}r{3pt}){5-6}
  & Event & No event & d & 2.5\% & 97.5\% & p\\
\midrule
\endhead

\endfoot
\bottomrule
\endlastfoot
Age & 63.8 & 59.1 & -4.73 & -6.65 & -2.81 & 0\\
BMI & 23.8 & 25.1 & 1.29 & 0.71 & 1.86 & 0\\
GCS - E & 3.3 & 3.9 & 0.61 & 0.46 & 0.76 & 0\\
GCS - M & 5.2 & 5.9 & 0.64 & 0.48 & 0.81 & 0\\*
\end{longtable}


\begin{longtable}[t]{llrrrrrrr}
\caption{\label{tab:cat.res}Descriptive statistics on categorical variables}\\
\toprule
\multicolumn{6}{c}{ } & \multicolumn{2}{c}{95\% CI} & \multicolumn{1}{c}{ } \\
\cmidrule(l{3pt}r{3pt}){7-8}
  & Status & Event & No event & Total & RR & 2.5\% & 97.5\% & p\\
\midrule
\endfirsthead
\caption[]{Descriptive statistics on categorical variables \textit{(continued)}}\\
\toprule
\multicolumn{6}{c}{ } & \multicolumn{2}{c}{95\% CI} & \multicolumn{1}{c}{ } \\
\cmidrule(l{3pt}r{3pt}){7-8}
  & Status & Event & No event & Total & RR & 2.5\% & 97.5\% & p\\
\midrule
\endhead

\endfoot
\bottomrule
\endlastfoot
Heart Disease & No & 54 & 2426 & 2480 & 1.00 & - & - & -\\
Heart Disease & Yes & 100 & 899 & 999 & 4.60 & 3.33 & 6.35 & 0\\
Renal Disease & No & 94 & 2964 & 3058 & 1.00 & - & - & -\\
Renal Disease & Yes & 60 & 361 & 421 & 4.64 & 3.41 & 6.31 & 0\\
Verbal Coherence & No & 81 & 430 & 511 & 1.00 & - & - & -\\
\addlinespace
Verbal Coherence & Yes & 73 & 2895 & 2968 & 0.16 & 0.11 & 0.21 & 0\\*
\end{longtable}

## Logistic Regression

Patients included in this analysis are 59.28 $\pm$
11.14 years old, with a BMI of 25.04 $\pm$ 3.86
kg/m^2^. Our model perform with $r^2=$ 0.13, calculated using
the variance-function-based approximation. Presented below is a table on model
summary:


\begin{longtable}[t]{lrrrr}
\caption{\label{tab:logreg}Summary on the logistic regression result}\\
\toprule
\multicolumn{2}{c}{ } & \multicolumn{2}{c}{95\% CI} & \multicolumn{1}{c}{ } \\
\cmidrule(l{3pt}r{3pt}){3-4}
  & OR & 2.5\% & 97.5\% & p\\
\midrule
\endfirsthead
\caption[]{Summary on the logistic regression result \textit{(continued)}}\\
\toprule
\multicolumn{2}{c}{ } & \multicolumn{2}{c}{95\% CI} & \multicolumn{1}{c}{ } \\
\cmidrule(l{3pt}r{3pt}){3-4}
  & OR & 2.5\% & 97.5\% & p\\
\midrule
\endhead

\endfoot
\bottomrule
\endlastfoot
Intercept & 4.22 & 0.52 & 35.05 & 0.18\\
Age & 1.02 & 1.00 & 1.04 & 0.02\\
Heart Disease & 2.63 & 1.80 & 3.87 & 0.00\\
Renal Disease & 2.75 & 1.85 & 4.06 & 0.00\\
GCS - E & 0.52 & 0.38 & 0.69 & 0.00\\
\addlinespace
GCS - M & 0.73 & 0.55 & 0.97 & 0.03\\
Verbal Coherence & 0.42 & 0.27 & 0.67 & 0.00\\
BMI & 0.94 & 0.89 & 0.99 & 0.02\\*
\end{longtable}

# Reference
