# 2023-07-09

- Demonstrate whether missingness contributes to bias:
  - Perform MICE imputation
  - Perform sensitivity analysis as a response to reviewer #2's concern
- Create a supplementary material based on the log of analysis
- Add an overall column to table 1 $\to$ Descriptive statistics for all the data, not grouped by mortality
- Change table 1, GCS should be continuous
- Create a Kaplan-Meier curve for time-to-event without any modelling (no dependent variables)
