## LOAD PACKAGES

pkgs <- c("magrittr", "gtsummary")
pkgs.load <- sapply(pkgs, library, character.only = TRUE)


## PREP

# Load data frame
sub.tbl <- readr::read_csv("data/processed/sub-tbl.csv")

# Set factor for sex and outcome
sub.tbl %<>%
  inset(c("outcome", "male"), value = list(
    .$outcome %>% factor(labels = c("Alive", "Deceased")),
    .$sex_ps == 2
  ))


## EDA

# Profiling the overall characteristics
sum.overall <- sub.tbl %>%
  select(!c(dist, sex_ps, outcome)) %>%
  tbl_summary(
    label = list(
      male = "Male",
      umur_ps = "Age",
      sex_ps  = "Sex",
      heart.disease = "Heart disease",
      renal.disease = "Renal disease",
      DM.uncontrolled = "Uncontrolled diabetes",
      HT.uncontrolled = "Uncontrolled hypertension",
      sistol = "Systole",
      diastol = "Diastole",
      E = "GCS - Eye",
      M = "GCS - Movement",
      V.coherent = "Verbal coherence",
      as_urat = "Uric acid",
      imt = "BMI",
      lama_rawat = "LoS",
      nihss_msk = "NIHSS",
      trigliserida = "Trigliceryde",
      ldl = "LDL",
      hdl = "HDL",
      kol_total = "Total cholesterol"
    ),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]",
      all_categorical() ~ "{n} ({p}%)"
    ),
    digits = all_continuous() ~ 2
  )

# Profiling the baseline characteristics, grouped by outcome
sum.outcome <- sub.tbl %>%
  select(!c(dist, sex_ps)) %>%
  tbl_summary(
    by = outcome,
    label = list(
      male = "Male",
      umur_ps = "Age",
      sex_ps  = "Sex",
      heart.disease = "Heart disease",
      renal.disease = "Renal disease",
      DM.uncontrolled = "Uncontrolled diabetes",
      HT.uncontrolled = "Uncontrolled hypertension",
      sistol = "Systole",
      diastol = "Diastole",
      E = "GCS - Eye",
      M = "GCS - Movement",
      V.coherent = "Verbal coherence",
      as_urat = "Uric acid",
      imt = "BMI",
      lama_rawat = "LoS",
      nihss_msk = "NIHSS",
      trigliserida = "Trigliceryde",
      ldl = "LDL",
      hdl = "HDL",
      kol_total = "Total cholesterol"
    ),
    statistic = list(
      all_continuous() ~ "{mean} [{sd}]",
      all_categorical() ~ "{n} ({p}%)"
    ),
    digits = all_continuous() ~ 2
  ) %>%
  add_difference(
    test = list(
      all_continuous() ~ "t.test",
      all_categorical() ~ "prop.test"
    )
  )

# Merge both table summaries of baseline characteristics
sum.baseline <- tbl_merge(
  tbls = list(sum.overall, sum.outcome),
  tab_spanner = c("**Overall**", "**Grouped by Event**")
)

sum.baseline %>% as_hux_table()

# Profiling the age and BMI for all subjects
sub.tbl %>% select(c(umur_ps, imt)) %>%
  tbl_summary(
    statistic = list(
      all_continuous() ~ "{mean} {sd}"
    ),
    digits = all_continuous() ~ 2
  ) %>%
  as_hux_table()


## WRITE

save("sum.baseline", file = "RData/eda.RData")
